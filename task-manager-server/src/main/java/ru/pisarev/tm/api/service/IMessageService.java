package ru.pisarev.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.dto.LoggerDTO;

public interface IMessageService {

    void sendMessage(@NotNull LoggerDTO entity);

    @SneakyThrows
    LoggerDTO prepareMessage(@Nullable Object record,
                             @NotNull String type);
}
