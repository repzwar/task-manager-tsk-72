package ru.pisarev.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    @Nullable String getUrl();

}
