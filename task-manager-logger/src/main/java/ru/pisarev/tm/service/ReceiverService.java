package ru.pisarev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pisarev.tm.api.service.IReceiverService;

import javax.jms.*;

import static ru.pisarev.tm.constant.ActiveMQConst.STRING;

@Service
public class ReceiverService implements IReceiverService {

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @Override
    @SneakyThrows
    public void receive(@NotNull @Autowired final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();

        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(STRING);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
