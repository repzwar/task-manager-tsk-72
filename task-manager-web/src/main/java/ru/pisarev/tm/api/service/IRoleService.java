package ru.pisarev.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.model.Role;

import java.util.List;

public interface IRoleService extends IRecordService<Role> {
    @NotNull
    @SneakyThrows
    List<Role> findAllByUserId(@NotNull String userId);

    @SneakyThrows
    void clear(@NotNull String userId);
}
