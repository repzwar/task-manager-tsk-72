<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project list</h1>

<table width="100%" cellpadding="10" border="1" style="margin-top: 20px">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap" align="left">Name</th>
        <th width="200" nowrap="nowrap">Desc</th>
        <th width="200" nowrap="nowrap">Start Date</th>
        <th width="200" nowrap="nowrap">Finish Date</th>
        <th width="200" nowrap="nowrap">Status</th>
        <th width="200" nowrap="nowrap" align="center">Edit</th>
        <th width="200" nowrap="nowrap" align="center">Delete</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${project.startDate}" pattern="dd.MM.yyyy"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${project.finishDate}" pattern="dd.MM.yyyy"/>
            </td>
            <td align="center">
                <c:out value="${project.status.getDisplayName()}"/>
            </td>
            <td align="center">
                <a href="/project/edit/${project.id}">Edit</a>
            </td>
            <td align="center">
                <a href="/project/delete/${project.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin-top: 20px">
    <button>Create Project</button>
</form>

<jsp:include page="../include/_footer.jsp"/>