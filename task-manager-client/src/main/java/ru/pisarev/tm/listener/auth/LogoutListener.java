package ru.pisarev.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.SessionEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.AuthAbstractListener;

@Component
public class LogoutListener extends AuthAbstractListener {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout from to application.";
    }

    @Override
    @EventListener(condition = "@logoutListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        sessionEndpoint.close(getSession());
    }
}
